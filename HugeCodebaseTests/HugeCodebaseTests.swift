//
//  HugeCodebaseTests.swift
//  HugeCodebaseTests
//
//  Created by Kubilay Erdogan on 2021-07-16.
//

import XCTest
import HugeCodebase

class HugeCodebaseTests: XCTestCase {
    func test_AnObject_initsWithGivenCase() {
        let givenCase: AnEnum = .anotherCase
        let sut = AnObject.init(aValue: AValue(), anEnum: givenCase)
        XCTAssertEqual(sut.anEnum, givenCase)
    }
}
