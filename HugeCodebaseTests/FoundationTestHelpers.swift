//
//  FoundationTestHelpers.swift
//  HugeCodebase
//
//  Created by Kubilay Erdogan on 2021-07-16.
//

import Foundation

public class FoundationTestHelpers {
    public static func aRandomInteger() -> Int {
        return Int.random(in: .zero...Int.max)
    }
}
