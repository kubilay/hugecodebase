//
//  DomainTestHelpers.swift
//  HugeCodebaseTests
//
//  Created by Kubilay Erdogan on 2021-07-16.
//

import HugeCodebase

public class DomainTestHelpers {
    public static func aRandomObject(forACase aCase: AnEnum) -> AnObject {
        return AnObject.init(aValue: AValue(), anEnum: aCase)
    }
}
