//
//  Definitions.swift
//  HugeCodebase
//
//  Created by Kubilay Erdogan on 2021-07-16.
//

import Foundation

public class AnObject {
    public let aValue: AValue
    public let anEnum: AnEnum

    public init(aValue: AValue, anEnum: AnEnum) {
        self.aValue = aValue
        self.anEnum = anEnum
    }
}

public struct AValue {
    public init() { }
}

public enum AnEnum {
    case aCase, anotherCase
}
