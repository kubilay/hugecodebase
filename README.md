## HugeCodebase
![](project.png)
### Status quo
- There's a main module (which is an app) called `HugeCodebase` which is too large and it takes a while to compile it
- There's a unit test target `HugeCodebaseTests` that contains test helpers like doubles, builders, mock data, etc.
- `HugeCodebase` defines `AnObject` class
- `HugeCodebaseTests` has a test helper called `DomainTestHelpers` which has `aRandomObject` method returning an object of type `AnObject`

### The problem
- It takes too long to run tests on `HugeCodebaseTests` (since it has `HugeCodebase` as dependency)

### The approach
- `HugeCodebase` has "Defines Module" build setting enabled so that it can be imported to use the types defined in it
- `HugeCodebaseTests` has "Defines Module" build setting enabled so that it can be imported to use the helpers defined in it
- There's another framework called `TDDSpace` which is empty and has no dependencies, only has its own unit test target (`TDDSpaceTests`)
- `TDDSpace` and `TDDSpaceTests` can import main module and/or its test target to be able to use the `HugeCodebase` types as well as `HugeCodebaseTests` test helpers

### An error in this setup
- When `TDDSpace` framework imports `HugeCodebase` module (the main module) and tries to create an instance of `AnObject`, there's a compiler error:
> Undefined symbol: nominal type descriptor for HugeCodebase.AnObject

### Another error in this setup
- When `TDDSpaceTests` framework imports `HugeCodebase` to use `AnObject` type and `HugeCodebaseTests` to use `DomainTestHelpers.aRandomObject` method, there are compiler errors:
> Undefined symbol: type metadata accessor for HugeCodebaseTests.DomainTestHelpers
>
> Undefined symbol: static HugeCodebaseTests.DomainTestHelpers.aRandomObject(forACase: HugeCodebase.AnEnum) -> HugeCodebase.AnObject

### When this setup works:
- When using Foundation types, for instance, there's a class and a method defined in `HugeCodebaseTests`: `FoundationTestHelpers.aRandomInteger` (which returns a random `Int`)
  - **Only when `FoundationTestHelpers.swift` has `TDDSpaceTests` target checked in its target membership section**
- If `FoundationTestHelpers.aRandomInteger` is called from `TDDSpaceTests`, it compiles and runs successfully.
