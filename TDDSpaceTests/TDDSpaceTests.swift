//
//  TDDSpaceTests.swift
//  TDDSpaceTests
//
//  Created by Kubilay Erdogan on 2021-07-16.
//

import XCTest
import TDDSpace
import HugeCodebase
import HugeCodebaseTests

class TDDSpaceTests: XCTestCase {
    func test_domainObjects() {
//        let _ = DomainTestHelpers.aRandomObject(forACase: .aCase)
        // Compiler error:
        // Undefined symbol: type metadata accessor for HugeCodebaseTests.DomainTestHelpers
        // Undefined symbol: static HugeCodebaseTests.DomainTestHelpers.aRandomObject(forACase: HugeCodebase.AnEnum) -> HugeCodebase.AnObject
    }

    func test_foundationObjects() {
        let _ = FoundationTestHelpers.aRandomInteger()
    }
}
